class AppSettingsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_app_setting, only: [:update]
  before_action :check_access, only: [:index, :create, :update, :destroy]

  def index
    @app_settings = AppSetting.all
    render :json => @app_settings.to_json
  end

  def new
  end

  def create
  end

  def show
  end

  def edit
  end

  def update
    answer = {error: false, message: "Успех"}
    @app_setting.update(app_setting_params[:app_setting])
    unless @app_setting.errors.empty?
      answer[:error] = !@app_setting.errors.empty?
      answer[:message] = @app_setting.errors
    end
    render :json => answer.to_json
  end

  def destroy
  end

  private

  def find_app_setting
    @app_setting = AppSetting.find(params[:id])
  end

  def app_setting_params
    params.required(:data).permit!
  end

  def check_access
    show_roles = ["administrator","manager"]
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
