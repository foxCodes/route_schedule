class AccountsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_account, only: [:update, :destroy]
  before_action :check_access, only: [:index, :create, :update, :destroy, :create_password, :role_index]

  def index
    @accounts = Account.all.map do |account|
      {
          :account => account,
          :role_ids => account.role_ids,
          :organization_ids => account.organization_ids,
          :roles_name => account.roles.collect(&:name).to_s.gsub(/"|\[|\]/,'')
      }
    end
    render :json => @accounts.to_json
  end

  def new
  end

  def create
    answer = {error: false, message: "Успех"}
    @account = Account.create(account_params[:account])
    if @account.errors.empty?
      @account.role_ids = account_params[:role_ids]
      @account.organization_ids = account_params[:organization_ids]
    else
      answer[:error] = !@account.errors.empty?
      answer[:message] = @account.errors
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
  end

  def update
    answer = {error: false, message: "Успех"}
    @account.update(account_params[:account])
    if @account.errors.empty?
      @account.role_ids = account_params[:role_ids]
      @account.organization_ids = account_params[:organization_ids]
    else
      answer[:error] = !@account.errors.empty?
      answer[:message] = @account.errors
    end
    render :json => answer.to_json
  end

  def destroy
    answer = {error: false, message: "Успех"}
    @account.destroy
    unless @account.errors.empty?
      answer[:error] = !@account.errors.empty?
      answer[:message] = @account.errors
    end
    render :json => answer.to_json
  end

  def create_password
    begin
      answer = {error: false, message: "Успех"}
      answer[:data] = Account.gen_password(1,2,2,3)
    rescue
      answer = {error: true, message: "Ошибка 500"}
    ensure
    end
    render :json => answer.to_json
  end

  def role_index
    @roles = Role.all
    render :json => @roles.to_json
  end

=begin
  def auth_test_token
    return auth_url(account_email: 'viktoriahorusina370@gmail.com', account_token: 'qcn_ZBUJFebL9Q6kmGAN')
  end
=end

  private

  def find_account
    @account = Account.find(params[:id])
  end

  def account_params
    params.required(:accounts).permit!
  end

  def check_access
    show_roles = ["administrator","manager"]
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
