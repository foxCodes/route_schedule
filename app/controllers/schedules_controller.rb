class SchedulesController < ApplicationController
  before_action :authenticate_account!
  before_action :find_schedule, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index, :create, :update, :destroy]

  def index
    if current_account.organizations.length>0
      @schedules = Schedule.where(organization_id: current_account.organizations.first.id).order(name: :asc).map do |schedule|
        {
            :schedule => schedule,
            :route => schedule.route,
        }
      end
    else
      @schedules = []
    end
    render :json => @schedules.to_json
  end

  def new
  end

  def create
    answer = {error: false, message: "Успех", id: nil}
    @schedule = Schedule.create(schedule_params)
    if @schedule.errors.empty?
      answer[:id] = @schedule.id
      CheckPoint.create({ name: "Контрольная точка", time_next_point: 0, number_pint: 0, schedule_id: @schedule.id})
    else
      answer[:error] = !@schedule.errors.empty?
      answer[:message] = @schedule.errors
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
  end

  def update
    answer = {error: false, message: "Успех"}
    @schedule.update(schedule_params[:schedule])
    unless @schedule.errors.empty?
      answer[:error] = !@schedule.errors.empty?
      answer[:message] = @schedule.errors
    end
    render :json => answer.to_json
  end

  def destroy
    answer = {error: false, message: "Успех"}
    @check_points = @schedule.check_points
    @check_points.delete_all if @check_points.length>0
    if @check_points.length==0
      @schedule.destroy
      unless @schedule.errors.empty?
        answer[:error] = !@schedule.errors.empty?
        answer[:message] = @schedule.errors
      end
    end
    render :json => answer.to_json
  end

  private

  def find_schedule
    @schedule = Schedule.find(params[:id])
  end

  def schedule_params
    params.required(:data).permit!
  end

  def check_access
    show_roles = ["administrator","manager","owner"]
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
