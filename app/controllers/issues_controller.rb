class IssuesController < ApplicationController
  before_action :authenticate_account!
  before_action :find_issue, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index, :index_by_sign ,:create, :update, :destroy, :destroy_by, :generation]

  def index
    @issues = Issue.all
    render :json => @issues.to_json
  end

  def index_by_sign
    @issues = Issue.where(schedule_id: params[:id]).order(:number)
    render :json => @issues.to_json
  end

  def new
  end

  def create
    answer = {error: false, message: "Успех"}
    @issue = Issue.create(issue_params[:issue])
    unless @issue.errors.empty?
      answer[:error] = !@issue.errors.empty?
      answer[:message] = @issue.errors
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
    render :json => @issue.to_json
  end

  def update
    answer = {error: false, message: "Успех"}
    @issue.update(issue_params[:issue])
    unless @issue.errors.empty?
      answer[:error] = !@issue.errors.empty?
      answer[:message] = @issue.errors
    end
    render :json => answer.to_json
  end

  def destroy
    answer = {error: false, message: "Успех"}
    @issue.destroy
    unless @issue.errors.empty?
      answer[:error] = !@issue.errors.empty?
      answer[:message] = @issue.errors
    end
    render :json => answer.to_json
  end

  def destroy_by
    answer = {error: false, message: "Успех"}
    @issues = Issue.where(schedule_id: params[:schedule_id])
    unless @issues.nil?
      @issues.delete_all
      unless @issues.length == 0
        answer[:error] = @issues.length == 0
        answer[:message] = @issues.errors
      end
    end
    render :json => answer.to_json
  end

  def generation
    begin
      answer = {error: false, message: "Успех"}
      @app_setting = AppSetting.last
      @schedule = Schedule.find(params[:id])
      begin_time = DateTime.parse(@schedule.begin_time.to_s)
      start_date = begin_time.to_i #seconds
      add_minutes = @schedule.time_circle /  @schedule.count_issue
      add_seconds = add_minutes*60
      set_first_start_side = (@schedule.count_issue%2==0 ? @schedule.count_issue/2 : @schedule.count_issue/2+1)
      break_circle = (@app_setting.default_break_circle.nil? ? nil : @app_setting.default_break_circle)
      count_replacement_issues = (@app_setting.count_replacement_issues.nil? ? 0 : @schedule.count_issue - @app_setting.count_replacement_issues)

      for a in 1..@schedule.count_issue do
        @issue = Issue.create({ number: a,
                                start_side: (a <= set_first_start_side || (a > count_replacement_issues && count_replacement_issues != 0) ? 'Начальная конт. точка' : 'Конечная конт. точка'),
                                break_circle: (a != 1 && (a <= count_replacement_issues || count_replacement_issues == 0) ? break_circle : nil),
                                count_circle: (a <= count_replacement_issues || count_replacement_issues == 0 ? @app_setting.default_count_circle : nil),
                                start_time: Time.at(start_date),
                                replace_issue: nil,
                                is_active: true,
                                schedule_id: @schedule.id
                              })
        start_date += add_seconds
      end
    rescue
      answer = {error: true, message: "Ошибка 500"}
    ensure
    end
    render :json => answer.to_json
  end

  private

  def find_issue
    @issue = Issue.find(params[:id])
  end

  def issue_params
    params.required(:data).permit!
  end

  def check_access
    show_roles = ["administrator","manager","owner"]
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
