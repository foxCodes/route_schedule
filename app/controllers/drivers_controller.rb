class DriversController < ApplicationController
  before_action :authenticate_account!
  before_action :find_driver, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index, :index_by_sign ,:create, :update, :destroy]

  def index
    #.where(organization_id: current_account.organizations.first.id)
    @drivers = Driver.all.order(name: :asc).map do |driver|
      {
        :driver => driver,
        :car_ids => driver.car_ids,
      }
    end
    render :json => @drivers.to_json
  end

  def index_by_sign
    @drivers = []
    if params[:class] == "organization"
      @drivers = Organization.find(params[:id]).drivers
    elsif params[:class] == "car"
      @drivers = Car.find(params[:id]).drivers
    end
    render :json => @drivers.to_json
  end

  def new
    @driver = Driver.new
  end

  def create
    @driver = Driver.create(driver_params[:driver])
    answer = @driver.errors.empty?
    @driver.car_ids = driver_params[:car_ids] if answer
    render :json => answer.to_json
  end

  def show
  end

  def edit
    render :json => @driver.to_json
  end

  def update
    @driver.update(driver_params[:driver])
    answer = @driver.errors.empty?
    @driver.car_ids = driver_params[:car_ids] if answer
    render :json => answer.to_json
  end

  def destroy
    @driver.destroy
    answer = @driver.errors.empty?
    render :json => answer.to_json
  end

  private

  def find_driver
    @driver = Driver.find(params[:id])
  end

  def driver_params
    params.required(:data).permit!
  end

  def check_access
    answer = "У Вас недостаточно прав!"
    show_roles = ["administrator","manager","owner"]
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
