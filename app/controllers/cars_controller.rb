class CarsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_car, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index, :index_by_sign ,:create, :update, :destroy]

  def index
    #.where(organization_id: current_account.organizations.first.id)
    @cars = Car.all.map do |car|
      {
        :car => car,
        :driver_ids => car.driver_ids,
        :route_ids => car.route_ids
      }
    end
    render :json => @cars.to_json
  end

  def index_by_sign
    @cars = []
    if params[:class] == "organization"
      @cars = Organization.find(params[:id]).cars
    elsif params[:class] == "driver"
      @cars = Driver.find(params[:id]).cars
    end
    render :json => @cars.to_json
  end

  def new
  end

  def create
    @car = Car.create(car_params[:car])
    answer = @car.errors.empty?
    if answer
      @car.driver_ids = car_params[:driver_ids]
      @car.route_ids = car_params[:route_ids]
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
  end

  def update
    @car.update(car_params[:car])
    answer = @car.errors.empty?
    if answer
      @car.driver_ids = car_params[:driver_ids]
      @car.route_ids = car_params[:route_ids]
    end
    render :json => answer.to_json
  end

  def destroy
    @car.destroy
    answer = @car.errors.empty?
    render :json => answer.to_json
  end

  private

  def find_car
    @car = Car.find(params[:id])
  end

  def car_params
    params.required(:data).permit!
  end

  def check_access
    answer = "У Вас недостаточно прав!"
    show_roles = ["administrator","manager","owner"]
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
