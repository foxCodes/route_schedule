class CheckPointsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_check_point, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index, :index_by_sign ,:create, :update, :destroy]

  def index
    @check_points = CheckPoint.all
    render :json => @check_points.to_json
  end

  def index_by_sign
    @check_points = CheckPoint.where(schedule_id: params[:id])
    render :json => @check_points.to_json
  end

  def new
  end

  def create
    answer = {error: false, message: "Успех"}
    @check_point = CheckPoint.create(check_point_params[:check_point])
    unless @check_point.errors.empty?
      answer[:error] = !@check_point.errors.empty?
      answer[:message] = @check_point.errors
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
  end

  def update
    answer = {error: false, message: "Успех"}
    @check_point.update(check_point_params[:check_point])
    unless @check_point.errors.empty?
      answer[:error] = !@check_point.errors.empty?
      answer[:message] = @check_point.errors
    end
    render :json => answer.to_json
  end

  def destroy
    answer = {error: false, message: "Успех"}
    @check_point.destroy
    unless @check_point.errors.empty?
      answer[:error] = !@check_point.errors.empty?
      answer[:message] = @check_point.errors
    end
    render :json => answer.to_json
  end

  private

  def find_check_point
    @check_point = CheckPoint.find(params[:id])
  end

  def check_point_params
    params.required(:data).permit!
  end

  def check_access
    show_roles = ["administrator","manager","owner"]
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end