class RoutesController < ApplicationController
  before_action :authenticate_account!
  before_action :find_route, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index, :index_by_organization ,:create, :update, :destroy]

  def index
    @routes = Route.all.order(name: :asc)
    render :json => @routes.to_json
  end

  def index_by_sign
    @routes = []
    if params[:class] == "organization"
      @routes = Organization.find(params[:id]).routes
    end
    render :json => @routes.to_json
  end

  def new
  end

  def create
    answer = {error: false, message: "Успех"}
    @route = Route.create(route_params[:route])
    unless @route.errors.empty?
      answer[:error] = !@route.errors.empty?
      answer[:message] = @route.errors
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
  end

  def update
    answer = {error: false, message: "Успех"}
    @route.update(route_params[:route])
    unless @route.errors.empty?
      answer[:error] = !@route.errors.empty?
      answer[:message] = @route.errors
    end
    render :json => answer.to_json
  end

  def destroy
    answer = {error: false, message: "Успех"}
    @route.destroy
    unless @route.errors.empty?
      answer[:error] = !@route.errors.empty?
      answer[:message] = @route.errors
    end
    render :json => answer.to_json
  end

  private

  def find_route
    @route = Route.find(params[:id])
  end

  def route_params
    params.required(:route).permit!
  end

  def check_access
    show_roles = []
    show_roles << "owner" if ["index", "index_by_sign"].include?(params[:action])
    show_roles << "administrator" if ["index", "index_by_organization" ,"create", "update", "destroy"].include?(params[:action])
    show_roles << "manager" if ["index", "index_by_organization" ,"create", "update", "destroy"].include?(params[:action])
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
