class ApplicationController < ActionController::Base
  before_action :authenticate_account!

  def index
  end

  def after_sign_in_path_for(resource_or_scope)
    '/app#/schedule'
  end

  def current_acccout_date
    render :json => [current_account].map{|account| {
                                                    :id => account.id,
                                                    :name => account.name,
                                                    :surname => account.surname,
                                                    :patronymic => account.patronymic,
                                                    :email => account.email,
                                                    :roles => account.roles.map{|r| r[:spec_name]},
                                                    :orgnizations => account.organizations.map{|o| o[:id]}
                                                    }
    }.to_json
  end

=begin
	def after_sign_in_path_for(resource)
		root_path
	end
=end
end
