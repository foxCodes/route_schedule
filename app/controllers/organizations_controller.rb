class OrganizationsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_organization, only: [:show, :edit, :update, :destroy]
  before_action :check_access, only: [:index ,:create, :update, :destroy, :destroy_by, :generation]

  def index
    @organizations = Organization.all.order(name: :asc)
    render :json => @organizations.to_json
  end

  def new
    @organization = Organization.new
  end

  def create
    answer = {error: false, message: "Успех"}
    @organization = Organization.create(organization_params[:organization])
    unless @organization.errors.empty?
      answer[:error] = !@organization.errors.empty?
      answer[:message] = @organization.errors
    end
    render :json => answer.to_json
  end

  def show
  end

  def edit
     render :json => @organization.to_json
  end

  def update
    answer = {error: false, message: "Успех"}
    @organization.update(organization_params[:organization])
    unless @organization.errors.empty?
      answer[:error] = !@organization.errors.empty?
      answer[:message] = @organization.errors
    end
    render :json => answer.to_json
  end

  def destroy
    answer = {error: false, message: "Успех"}
    @organization.destroy
    unless @organization.errors.empty?
      answer[:error] = !@organization.errors.empty?
      answer[:message] = @organization.errors
    end
    render :json => answer.to_json
  end

  private

  def find_organization
    @organization = Organization.find(params[:id])
  end

  def organization_params
    params.required(:organization).permit!
  end

  def check_access
    show_roles = ["administrator","manager"]
    answer = {error: true, message: "У Вас недостаточно прав"}
    if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
      render :json => answer.to_json
    end
  end
end
