import axios from 'axios'

const carStore = {
	state:{
	  cars: [],
      car: {},
      car_dialog: false,
	},
	mutations:{
		CHANGE_CAR_DIALOG: (state) => {
			state.car_dialog = !state.car_dialog;
		},
		UPDATE_CAR: (state,cars) => {
			state.cars = cars;
		},
		DELETE_CAR: (state,index) => {
			state.cars.splice(index,1);
		},
	},
	actions: {
		CHANGE_CAR_DIALOG({commit}){
			commit('CHANGE_CAR_DIALOG');
		},
		LOAD_CARS_BY_SIGN({commit},arg_arr){
			return axios({
						   method: 'get',
					       url: `/cars_by_sign?class=${arg_arr[0]}&id=${arg_arr[1]}`
						}).then(function (response) {
						   commit('UPDATE_CAR', response.data);
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /cars_by_sign");
						   return error;
						});
		},
		LOAD_CARS({commit}){
			return axios({
						   method: 'get',
					       url: '/cars'
						}).then(function (response) {
							commit('UPDATE_CAR', response.data);
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /cars/index");
						   return error;
						});
		},
		CREATE_CAR({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/cars',
					       data: {data: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /cars/create");
						   return error;
						});
		},
		UPDATE_CAR({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: `/cars/${arg_arr[0]}`,
						   data: {data: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /cars/update");
						   return error;
						});
		},
		DESTROY_CAR({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: `/cars/${arg_arr[0]}`
						}).then(function (response) {
						   if(response.data){commit('DELETE_CAR', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /cars/destroy");
						   return error;
						});
		},
	},
	getters:{
		GET_CAR_DIALOG(state){
			return state.car_dialog;
		},
		GET_CARS(state){
			return state.cars;
		},
	}
}

export default carStore
