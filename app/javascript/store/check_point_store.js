import axios from 'axios'

const checkPointStore = {
	state:{
		check_points: [],
		check_point: {},
	},
	mutations:{
		UPDATE_CHECK_POINT: (state,check_points) => {
			state.check_points = check_points;
		},
		DELETE_CHECK_POINT: (state,number) => {
			state.check_points.map((bs, index) => {
				if(bs.number_pint == number){
					state.check_points.splice(index,1);
				}
			});
		},
	},
	actions: {
		LOAD_CHECK_POINTS_BY_SCHEDULE({commit},arg_arr){
			return axios({
						   method: 'get',
					       url: `/check_points_by_sign?id=${arg_arr[0]}`
						}).then(function (response) {
						   if(response.data.error){
								commit('UPDATE_CHECK_POINT',[]);
						   }else{
								commit('UPDATE_CHECK_POINT',response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /check_points_by_sign");
						   return error;
						});
		},
		CREATE_CHECK_POINT({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/check_points',
					       data: {data: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /check_points/create");
						   return error;
						});
		},
		UPDATE_CHECK_POINT({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: `/check_points/${arg_arr[0]}`,
						   data: {data: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /check_points/update");
						   return error;
						});
		},
		DESTROY_CHECK_POINT({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: `/check_points/${arg_arr[0]}`
						}).then(function (response) {
						   if(!response.data.error){commit('DELETE_CHECK_POINT', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /check_points/destroy");
						   return error;
						});
		},
	},
	getters:{
		GET_CHECK_POINTS(state){
			return state.check_points;
		},
	}
}

export default checkPointStore
