import axios from 'axios'

const issueStore = {
	state:{
	  issues: [],
	  issue: {},
	  issue_dialog: false,
	},
	mutations:{
		CHANGE_ISSUE_DIALOG: (state) => {
			state.issue_dialog = !state.issue_dialog;
		},
		UPDATE_ISSUE: (state,issues) => {
			state.issues = issues;
		},
		DELETE_ISSUE: (state,index) => {
			state.issues.splice(index,1);
		},
		DELETE_ISSUE_BY: (state) => {
			state.issues = [];
		},
	},
	actions: {
		CHANGE_ISSUE_DIALOG({commit}){
			commit('CHANGE_ISSUE_DIALOG');
		},
		LOAD_ISSUES_BY_SCHEDULE({commit},arg_arr){
			return axios({
						   method: 'get',
					       url: `/issues_by_sign?id=${arg_arr[0]}`
						}).then(function (response) {
						   if(response.data.error){
						      commit('UPDATE_ISSUE',[]);
						   }else{
						      commit('UPDATE_ISSUE',response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /issues_by_sign");
						   return error;
						});
		},
		LOAD_ISSUES({commit}){
			return axios({
						   method: 'get',
					       url: '/issues'
						}).then(function (response) {
						   if(response.data.error){
					          commit('UPDATE_ISSUE',[]);
				           }else{
					          commit('UPDATE_ISSUE',response.data);
				           }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /issues/index");
						   return error;
						});
		},
		CREATE_ISSUE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/issues',
					       data: {data: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /issues/create");
						   return error;
						});
		},
		GENERATION_ISSUES({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
							method: 'get',
							url: `/generation_issues/?id=${arg_arr[0]}`,
						}).then(function (response) {
							return response;
						}).catch(function (error) {
							console.log("Ошибка запроска к /generation_issues");
							return error;
						});
		},
		UPDATE_ISSUE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: `/issues/${arg_arr[0]}`,
						   data: {data: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /issues/update");
						   return error;
						});
		},
		DESTROY_ISSUE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: `/issues/${arg_arr[0]}`
						}).then(function (response) {
						   if(!response.data.error){commit('DELETE_ISSUE', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /issues/destroy");
						   return error;
						});
		},
		DESTROY_ISSUE_BY({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'get',
					       url: `/issue_destroy_by?schedule_id=${arg_arr[0]}`
						}).then(function (response) {
						   if(response.data){commit('DELETE_ISSUE_BY');}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /issue_destroy_by");
						   return error;
						});
		},
	},
	getters:{
		GET_ISSUES(state){
			return state.issues;
		},
	}
}

export default issueStore
