import axios from 'axios'

const routeStore = {
	state:{
		routes: [],
		route: {},
		route_dialog: false,
	},
	mutations:{
		CHANGE_ROUTE_DIALOG: (state) => {
			state.route_dialog = !state.route_dialog;
		},
		UPDATE_ROUTE: (state,routes) => {
			state.routes = routes;
		},
		DELETE_ROUTE: (state,index) => {
			state.routes.splice(index,1);
		},
	},
	actions: {
		CHANGE_ROUTE_DIALOG({commit}){
			commit('CHANGE_ROUTE_DIALOG');
		},
		LOAD_ROUTES({commit}){
			return axios({
						   method: 'get',
					       url: '/routes'
						}).then(function (response) {
						   if(response.data.error){
							   commit('UPDATE_ROUTE', []);
						   }else{
						      commit('UPDATE_ROUTE', response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /routes/index");
						   return error;
						});
		},
		LOAD_ROUTES_BY_SIGN({commit},arg_arr){
			return axios({
						   method: 'get',
						   url: `/routes_by_sign?class=${arg_arr[0]}&id=${arg_arr[1]}`
						}).then(function (response) {
						   if(response.data.error){
						      commit('UPDATE_ROUTE', []);
						   }else{
						      commit('UPDATE_ROUTE', response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /routes/index");
						   return error;
						});
		},
		CREATE_ROUTE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/routes',
					       data: {route: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /routes/create");
						   return error;
						});
		},
		UPDATE_ROUTE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: '/routes/' + arg_arr[0],
						   data: {route: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /routes/update");
						   return error;
						});
		},
		DESTROY_ROUTE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: '/routes/' + arg_arr[0]
						}).then(function (response) {
						   if(!response.data.error){commit('DELETE_ROUTE', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /routes/destroy");
						   return error;
						});
		},
	},
	getters:{
		GET_ROUTE_DIALOG(state){
			return state.route_dialog;
		},
		GET_ROUTES(state){
			return state.routes;
		},
	}
}

export default routeStore
