import axios from 'axios'

const accountStore = {
	state: {
		accounts: [],
		account: {},
		roles: [],
		password: '',
		account_dialog: false,
		account_sort: true,
	},
	mutations: {
		CHANGE_ACCOUNT_DIALOG: (state) => {
			state.account_dialog = !state.account_dialog;
		},
		CHANGE_ACCOUNT_SORT: (state) => {
			state.account_sort = !state.account_sort;
		},
		UPDATE_ACCOUNTS: (state,accounts) => {
			state.accounts = accounts;
		},
		DELETE_ACCOUNT: (state,index) => {
			state.accounts.splice(index,1);
		},
		UPDATE_PASSWORD: (state,password) => {
			state.password = password;
		},
		UPDATE_ROLES: (state,roles) => {
			state.roles = roles;
		},
	},
	actions: {
		CHANGE_ACCOUNT_DIALOG({commit}){
			commit('CHANGE_ACCOUNT_DIALOG');
		},
		CHANGE_ACCOUNT_SORT({commit}){
			commit('CHANGE_ACCOUNT_SORT');
		},
		LOAD_ACCOUNTS({commit}){
			return axios({
				           method: 'get',
						   url: '/accounts'
						}).then(function (response) {
						   if(response.data.error){
							   commit('UPDATE_ACCOUNTS',[]);
						   }else{
							   commit('UPDATE_ACCOUNTS',response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /accounts/index");
						   return error;
						});
		},
		CREATE_ACCOUNT({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1]
			return axios({
				           method: 'post',
					       url: '/accounts',
			               data: {accounts: arg_arr[0]}
			            }).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /accounts/create");
						   return error;
						});
		},
		UPDATE_ACCOUNT({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
				           method: 'put',
                           url: '/accounts/' + arg_arr[0],
                           data: {accounts: arg_arr[1]}
			            }).then(function (response) {
			               return response;
			            }).catch(function (error) {
			               console.log("Ошибка запроска к /accounts/update");
			               return error;
			            });
		},
		DESTROY_ACCOUNT({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
				           method: 'delete',
			               url: '/account/' + arg_arr[0]
			             }).then(function (response) {
						   if(!response.data.error){commit('DELETE_ACCOUNT', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /accounts/destroy");
						   return error;
						});
		},
		CREATE_PASSWORD({commit}){
			return axios({
				            method: 'get',
               			    url: '/create_password'
			             }).then(function (response) {
						    commit('UPDATE_PASSWORD',response.data.data);
						    return response;
			             }).catch(function (error) {
			             	console.log("Ошибка запроска к /create_password");
			             	return error;
			             });
		},
		LOAD_ROLES({commit}){
			return axios({
				            method: 'get',
			                url: '/roles'
			             }).then(function (response) {
							if(response.data.error){
								commit('UPDATE_ROLES',[]);
							}else{
								commit('UPDATE_ROLES',response.data);
							}
			                return response;
			             }).catch(function (error) {
			             	console.log("Ошибка запроска к /accounts/index");
			             	return error;
			             });
		},
	},
	getters: {
		GET_ACCOUNT_DIALOG(state){
			return state.account_dialog;
		},
		GET_ACCOUNT_SORT(state){
			return state.account_sort;
		},
		GET_ACCOUNTS(state){
			return state.accounts;
		},
		GET_PASSWORD(state){
			return state.password;
		},
		GET_ROLES(state){
			return state.roles;
		},
	}
}

export default accountStore
