import axios from 'axios'

const diverStore = {
	state:{
		drivers: [],
		driver: {},
		driver_dialog: false,
	},
	mutations:{
		CHANGE_DRIVER_DIALOG: (state) => {
			state.driver_dialog = !state.driver_dialog;
		},
		UPDATE_DRIVER: (state,drivers) => {
			state.drivers = drivers;
		},
		DELETE_DRIVER: (state,index) => {
			state.drivers.splice(index,1);
		},
	},
	actions: {
		CHANGE_DRIVER_DIALOG({commit}){
			commit('CHANGE_DRIVER_DIALOG');
		},
		LOAD_DRIVERS({commit}){
			return axios({
						   method: 'get',
					       url: '/drivers'
						}).then(function (response) {
						   commit('UPDATE_DRIVER', response.data);
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /drivers/index");
						   return error;
						});
		},
		LOAD_DRIVERS_BY_SIGN({commit},arg_arr){
			return axios({
			 				method: 'get',
							url: `/drivers_by_sign?class=${arg_arr[0]}&id=${arg_arr[1]}`
						}).then(function (response) {
							commit('UPDATE_DRIVER', response.data);
							return response;
						}).catch(function (error) {
							console.log("Ошибка запроска к /drivers_by_sign");
							return error;
						});
		},
		CREATE_DRIVER({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/drivers',
					       data: {data: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /drivers/create");
						   return error;
						});
		},
		UPDATE_DRIVER({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: '/drivers/' + arg_arr[0],
						   data: {data: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /drivers/update");
						   return error;
						});
		},
		DESTROY_DRIVER({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: '/drivers/' + arg_arr[0]
						}).then(function (response) {
						   if(response.data){commit('DELETE_DRIVER', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /drivers/destroy");
						   return error;
						});
		},
	},
	getters:{
		GET_DRIVER_DIALOG(state){
			return state.driver_dialog;
		},
		GET_DRIVERS(state){
			return state.drivers;
		},
	}
}

export default diverStore
