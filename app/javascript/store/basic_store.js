import axios from 'axios'

const basicStore = {
	state: {
		current_account: {},
		app_settings: [],
		sex_items: ['Мужской','Женский'],
	},
	mutations: {
		UPDATE_CURRENT_ACCOUNT: (state,current_account) => {
			state.current_account = current_account;
		},
		UPDATE_APP_SETTINGS: (state,app_settings) => {
			state.app_settings = app_settings;
		},
	},
	actions: {
		CURRENT_ACCOUNT({commit,state}){
			return axios({
				           method: 'get',
						   url: '/current_acccout_date'
						}).then(function (response) {
						   commit('UPDATE_CURRENT_ACCOUNT',response.data);
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /current_account_data");
						   return error;
						});
		},
		LOAD_APP_SETTINGS({commit,state}){
			return axios({
							method: 'get',
							url: '/app_settings'
						}).then(function (response) {
							if(response.data.error){
								commit('UPDATE_APP_SETTINGS',[]);
							}else{
								commit('UPDATE_APP_SETTINGS',response.data);
							}
							return response;
						}).catch(function (error) {
							console.log("Ошибка запроска к /app_settings");
							return error;
						});
		},
		UPDATE_APP_SETTINGS({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
							method: 'put',
							url: '/app_settings/' + arg_arr[0],
							data: {data: arg_arr[1]}
						}).then(function (response) {
							return response;
						}).catch(function (error) {
							console.log("Ошибка запроска к /app_settings/update");
							return error;
						});
		},
	},
	getters: {
		GET_ACCOUNT(state){
			return state.current_account;
		},
		GET_SEX_ITEMS(state){
			return state.sex_items;
		},
		GET_APP_SETTINGS(state){
			return state.app_settings;
		},
	}
}

export default basicStore
