import axios from 'axios'

const scheduleStore = {
	state:{
		schedules: [],
		schedule: {},
		schedule_dialog: false,
	},
	mutations:{
		CHANGE_SCHEDULE_DIALOG: (state) => {
			state.schedule_dialog = !state.schedule_dialog;
		},
		UPDATE_SCHEDULE: (state,schedules) => {
			state.schedules = schedules;
		},
		DELETE_SCHEDULE: (state,index) => {
			state.schedules.splice(index,1);
		},
	},
	actions: {
		CHANGE_SCHEDULE_DIALOG({commit}){
			commit('CHANGE_SCHEDULE_DIALOG');
		},
		LOAD_SCHEDULES({commit}){
			return axios({
						   method: 'get',
					       url: '/schedules'
						}).then(function (response) {
						   if(response.data.error){
						      commit('UPDATE_SCHEDULE',[]);
						   }else{
						   	  commit('UPDATE_SCHEDULE', response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /schedules/index");
						   return error;
						});
		},
		CREATE_SCHEDULE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/schedules',
					       data: {data: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /schedules/create");
						   return error;
						});
		},
		UPDATE_SCHEDULE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: '/schedules/' + arg_arr[0],
						   data: {data: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /schedules/update");
						   return error;
						});
		},
		DESTROY_SCHEDULE({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: '/schedules/' + arg_arr[0]
						}).then(function (response) {
						   if(!response.data.error){commit('DELETE_SCHEDULE', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /schedules/destroy");
						   return error;
						});
		},
	},
	getters:{
		GET_SCHEDULE_DIALOG(state){
			return state.schedule_dialog;
		},
		GET_SCHEDULES(state){
			return state.schedules;
		},
	}
}

export default scheduleStore
