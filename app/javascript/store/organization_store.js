import axios from 'axios'

const organizationStore = {
	state:{
		organizations: [],
		organization: {},
		organization_dialog: false,
	},
	mutations:{
		CHANGE_ORGANIZATION_DIALOG: (state) => {
			state.organization_dialog = !state.organization_dialog;
		},
		UPDATE_ORGANIZATION: (state,organizations) => {
			state.organizations = organizations;
		},
		DELETE_ORGANIZATION: (state,index) => {
			state.organizations.splice(index,1);
		},
	},
	actions: {
		CHANGE_ORGANIZATION_DIALOG({commit}){
			commit('CHANGE_ORGANIZATION_DIALOG');
		},
		LOAD_ORGANIZATIONS({commit}){
			return axios({
						   method: 'get',
					       url: '/organizations'
						}).then(function (response) {
						   if(response.data.error){
						      commit('UPDATE_ORGANIZATION',[]);
						   }else{
						      commit('UPDATE_ORGANIZATION',response.data);
						   }
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /organizations/index");
						   return error;
						});
		},
		CREATE_ORGANIZATION({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[1];
			return axios({
						   method: 'post',
					       url: '/organizations',
					       data: {organization: arg_arr[0]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /organizations/create");
						   return error;
						});
		},
		UPDATE_ORGANIZATION({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'put',
						   url: '/organizations/' + arg_arr[0],
						   data: {organization: arg_arr[1]}
						}).then(function (response) {
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /organizations/update");
						   return error;
						});
		},
		DESTROY_ORGANIZATION({commit},arg_arr){
			axios.defaults.headers.common['X-CSRF-TOKEN'] = arg_arr[2];
			return axios({
						   method: 'delete',
					       url: '/organizations/' + arg_arr[0]
						}).then(function (response) {
						   if(!response.data.error){commit('DELETE_ORGANIZATION', arg_arr[1]);}
						   return response;
						}).catch(function (error) {
						   console.log("Ошибка запроска к /organizations/destroy");
						   return error;
						});
		},
	},
	getters:{
		GET_ORGANIZATION_DIALOG(state){
			return state.organization_dialog;
		},
		GET_ORGANIZATIONS(state){
			return state.organizations;
		},
	}
}

export default organizationStore
