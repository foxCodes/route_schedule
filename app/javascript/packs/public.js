//import { createApp } from 'vue'
//import { createApp } from 'vue/dist/vue.runtime.esm-bundler';
import { createApp } from 'vue/dist/vue.esm-bundler';
import createVuetify from '../plugins/vuetify'

import PublicLayout from '../components/public/public_layout.vue'
import NewAccountSession from '../components/public/new_account_session.vue'


export default () => {
    document.addEventListener('DOMContentLoaded', () => {
        const app = createApp({
            data() {
                return {
                }
            }
        });

        app.component('new-account-session', NewAccountSession);
        app.component('public-layout', PublicLayout);
        app.use(createVuetify).mount('#app');
    });
}

/*

Vue.component('new-account-session', NewAccountSession);
Vue.component('public-layout', PublicLayout);

export default () => {
    document.addEventListener('DOMContentLoaded', () => {
        console.log("sd");
        let app=createApp(PublicLayout);
        app.use(createVuetify)
            .mount('#app1')
    })
}
*/

/*
document.addEventListener('DOMContentLoaded', () => {
	const element = document.getElementById('app');
    if (element != null) {
        const app = new Vue({
            vuetify,																																																																																																																																																																																					
        }).$mount(element);
    }


  /!*const el = document.body.appendChild(document.createElement('public'))
  const app = new Vue({
	el,
  	router,
  	vuetify,
  	store,
    render: h => h(PublicLayout)
  })*!/
});*/
