import { createApp } from 'vue'
import createVuetify from '../plugins/vuetify'
import createRouter from '../plugins/routes'
import createStore from '../plugins/store'
import Layout from '../components/base_layout.vue'
import _ from 'lodash'
import moment from 'moment'

moment.locale('ru');

export default () => {
    document.addEventListener('DOMContentLoaded', () => {
        let app=createApp(Layout);
        app.config.globalProperties.$moment=moment;

        app.use(createVuetify)
            .use(createRouter)
            .use(createStore)
            .use(_)
            .use(moment)
            .mount('#app')
    })
}
