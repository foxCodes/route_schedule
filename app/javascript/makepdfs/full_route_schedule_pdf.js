import moment from 'moment'

export default {
    createPDF(schedule,issues,points,route,app_settings,current_account){
        let current_date = new Date();
        let app_setting = (app_settings.length > 0 ? app_settings[0] : undefined);

        pdfMake.fonts = {
            Times: {
                normal: 'TimesNewRomanCyrRegular.TTF',
                bold: 'TimesNewRomanCyrBold.TTF',
                italics: 'TimesNewRomanCyrItalic.TTF',
                bolditalics: 'TimesNewRomanCyrBoldItalic.TTF'
            }
        };

        function buildColumn(points){
            let column = [];
            let one_column = (app_setting && app_setting.full_page_width ? app_setting.full_page_width : 450)/points.length;
            for(let i=0;i<points.length; i++){
                column.push(one_column)
            }
            return column
        }

        function getEmptyColumns(empty_column){
            let answer = []
            for(let c=0;c<empty_column;c++){
                answer.push({})
            }
            return answer
        }

        function getCutName(full_name){
            let cut_name_arr = full_name.split(' ');
            let cut_name = "";
            cut_name_arr.forEach(function(item , index) {
                cut_name += `${ item.substring(0, 3)}. `;
            });
            return cut_name
        }

        function buildTableBody(schedule, issue, points, prefix, postfix, replacement_issues){
           let total_table = {
                style: `table_${issue.number}`,
                margin: [ 0, 0, 0, 20 ],
                unbreakable: (app_setting && app_setting.stream_table_page ? false : true),
                table: {
                widths:  buildColumn(points),
                heights: [],
                body: []
                },
           };

           let dataHeadRow = [];
           let points_reverse = points.slice().reverse();
           let seconds_start = new Date(issue.start_time)/1000;

            total_table.table.body.push([].concat([{text: `Выпуск ${issue.number}`, colSpan: points.length, style: 'twelveBoldCenter', alignment: 'center'}], getEmptyColumns(points.length-1)));

            if(issue.start_side==undefined || issue.start_side=='Начальная конт. точка'){
                points.forEach(function(p){
                    dataHeadRow.push({text: getCutName(p.name.toString()), style: 'twelveBoldCenter', alignment: 'center', fillColor: '#cccccc'});
                });
            }else{
                points_reverse.forEach(function(p){
                    dataHeadRow.push({text: getCutName(p.name.toString()), style: 'twelveBoldCenter', alignment: 'center', fillColor: '#cccccc'});
                });
            }

            total_table.table.body.push(dataHeadRow);

            if(replacement_issues.length==0){
                for(let c=0;c<issue.count_circle;c++){
                    let dataBodyRow = [];
                    if(issue.break_circle - 1 != c){
                        if(issue.start_side==undefined || issue.start_side=='Начальная конт. точка'){
                            points.forEach(function(p){
                                dataBodyRow.push({text: moment(new Date(seconds_start*1000)).format('LT'), style: 'twelveNormalCenter', alignment: 'center'});
                                seconds_start += p.time_next_point*60;
                            });
                        }else{
                            let first_time_next_point = 0;
                            points_reverse.forEach(function(p,i){
                                dataBodyRow.push({text: moment(new Date(seconds_start*1000)).format('LT'), style: 'twelveNormalCenter', alignment: 'center'});
                                if(i==0){
                                    first_time_next_point = p.time_next_point;
                                    seconds_start += points_reverse[i+1].time_next_point*60;
                                }else if(i==points.length-1){
                                    seconds_start += first_time_next_point;
                                }else{
                                    seconds_start += points_reverse[i+1].time_next_point*60;
                                }
                            });
                        }
                        total_table.table.body.push(dataBodyRow);
                    }else{
                        total_table.table.body.push([].concat([{text: moment(new Date(seconds_start*1000)).format('LT'), style: 'twelveNormalCenter', alignment: 'center'},{text: 'перерыв', colSpan: points.length-1, style: 'twelveNormalCenter', alignment: 'center'}], getEmptyColumns(points.length-2)))
                        seconds_start += schedule.time_circle*60;
                    }
                }

                if(postfix != [] && issue.count_circle > 0){
                    postfix.forEach(function(row){
                        total_table.table.body.push([].concat([{text: moment(new Date(seconds_start*1000)).format('LT'), style: 'twelveNormalCenter', alignment: 'center'},{text: row, colSpan: points.length-1, style: 'twelveNormalCenter', alignment: 'center'}], getEmptyColumns(points.length-2)));
                    });
                }
            }else{
                replacement_issues.forEach(function(ri,ii){
                    let seconds_start = new Date(ri.start_time)/1000;
                    for(let c=0;c<ri.break_circle;c++){
                        let dataBodyRow = [];
                        points.forEach(function(p){
                            if(c==ri.break_circle-1){dataBodyRow.push({text: moment(new Date(seconds_start*1000)).format('LT'), style: 'twelveNormalCenter', alignment: 'center'});}
                            seconds_start += p.time_next_point*60;
                        });
                        if(dataBodyRow.length>0){total_table.table.body.push(dataBodyRow);}
                    }
                    total_table.table.body.push([].concat([{text: moment(new Date(seconds_start*1000)).format('LT'), style: 'twelveNormalCenter', alignment: 'center'},{text: (ii!=replacement_issues.length-1 ? 'перерыв' : 'конец смены'), colSpan: points.length-1, style: 'twelveNormalCenter', alignment: 'center'}], getEmptyColumns(points.length-2)))
                });
            }

            return total_table;
        }

        var titlePage = {
            info: {
                title: `Общие расписание. Маршрут ${route.name}. Сформировано ${moment(current_date).format('L LT')}`,
                author: `${current_account.surname} ${current_account.name} ${current_account.patronymic}`,
                subject: '',
                keywords: ''
            },

            pageSize: 'A4',
            pageOrientation: 'portrait',
            pageMargins:[10,10,10,10],
            background: {},
            header:[],
            footer:[],
            content:[
                {
                    style: 'tableHeader',
                    table: {
                        widths: [560],
                        heights: [3,20],
                        body: [
                            [{
                                text: `Расписание м-та № ${route.name}`,
                                border: [false, false, false, false],
                                style: 'sixteenBoldCenter',
                            }],
                            [{
                                text: `Действует с ${moment(schedule.begin_date).format('L')} по ${moment(schedule.end_date).format('L')}; ${(schedule.weekend ? 'Выходные дни' : 'Будние дни')}; Время кргуга: ${schedule.time_circle} минут;`,
                                border: [false, false, false, false],
                                style: 'twelveBoldCenter',
                            }],

                        ]
                    },
                },
            ],
            styles: {
                twelveNormalCenter: {
                    fontSize: 12,
                    font: 'Times',
                    bold: false,
                    alignment: 'center',
                },
                twelveBoldCenter: {
                    fontSize: 12,
                    font: 'Times',
                    bold: true,
                    alignment: 'center',
                },
                fourteenBoldCenter: {
                    fontSize: 14,
                    font: 'Times',
                    bold: true,
                    alignment: 'center',
                },
                sixteenBoldCenter: {
                    fontSize: 16,
                    font: 'Times',
                    bold: true,
                    alignment: 'center',
                },
            }
        };

        issues.forEach(function(issue){
           let replacement_issues = issues.filter((i) => {return i.replace_issue == issue.number});
           let table = buildTableBody(schedule, issue, points, [], ['конец смены'], replacement_issues);
           titlePage.content.push(table);
        });

        pdfMake.createPdf(titlePage).download(`Общие_расписание_маршрут_${route.name}_${moment(current_date).format('L_LT')}.pdf`);
    }
}