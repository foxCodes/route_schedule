import * as VueRouter from 'vue-router'

import Organization from '../components/organization'
import AppSetting from '../components/app_setting'
import Schedule from '../components/schedule'
import Account from '../components/account'
import Driver from '../components/driver'
import Route from '../components/route'
import Car from '../components/car'

const routes = [
    { path: '/organization', component: Organization },
    { path: '/schedule', component: Schedule },
    { path: '/setting', component: AppSetting },
    { path: '/account', component: Account },
    { path: '/driver', component: Driver },
    { path: '/route', component: Route },
    { path: '/car', component: Car },
]

export default VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes,
})