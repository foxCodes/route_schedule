//import 'vuetify/lib/styles/main.sass'
import { createVuetify } from 'vuetify'
import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
import * as components from 'vuetify/lib/components/index'
import * as directives from 'vuetify/lib/directives/index'

export default createVuetify({
    icons: {
        defaultSet: 'mdi'
    },
    components,
    directives
})