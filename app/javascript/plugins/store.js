/*import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import organizationStore from './stores/organization_store'
import importDataStore from './stores/import_data_store'
import waybillStore from './stores/waybill_store'
import accountStore from './stores/account_store'
import serviceStore from './stores/service_store'
import trailerStore from './stores/trailer_store'
import driverStore from './stores/driver_store'
import basicStore from './stores/basic_store'
import carStore from './stores/car_store'

const store = new Vuex.Store({
    modules: {
        organization: organizationStore,
        import_data: importDataStore,
        waybill: waybillStore,
        account: accountStore,
        service: serviceStore,
        trailer: trailerStore,
        driver: driverStore,
        basic: basicStore,
        car: carStore
    }
})

export default store
*/

import {createStore} from "vuex";

import organizationStore from '../store/organization_store'
import checkPointStore from "../store/check_point_store";
import scheduleStore from '../store/schedule_store'
import accountStore from '../store/account_store'
import driverStore from '../store/driver_store'
import issueStore from '../store/issue_store'
import routeStore from '../store/route_store'
import basicStore from '../store/basic_store'
import carStore from  '../store/car_store'

const store = createStore({
    modules: {
        organization: organizationStore,
        check_point: checkPointStore,
        schedule: scheduleStore,
        account: accountStore,
        driver: driverStore,
        issue: issueStore,
        route: routeStore,
        basic: basicStore,
        car: carStore
    }
})

export default store