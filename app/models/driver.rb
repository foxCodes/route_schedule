class Driver < ApplicationRecord
  belongs_to :organization, optional: true
  has_and_belongs_to_many :cars
end
