class Route < ApplicationRecord
  has_and_belongs_to_many :cars
  has_many :schedules
end