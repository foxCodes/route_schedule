class Account < ApplicationRecord
  has_and_belongs_to_many :roles
  has_and_belongs_to_many :organizations
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         #:registerable,
         :recoverable,
         :rememberable,
         :validatable

  def self.gen_password(count_sym,count_nub,count_alf_b,count_alf_l)
    current_password = []
    count_sym.times do
      current_password << ['$','#','=','+','/','*','-','@','(',')','_','>','<'].to_a.sample
    end
    count_nub.times do
      current_password << (0..100).to_a.sample
    end
    count_alf_b.times do
      current_password << ('A'..'Z').to_a.sample
    end
    count_alf_l.times do
      current_password << ('a'..'z').to_a.sample
    end
    password = current_password.shuffle.join
    return password
  end

  def self.auth_test_token
    return auth_url(account_email: 'viktoriahorusina370@gmail.com', account_token: 'qcn_ZBUJFebL9Q6kmGAN')
  end
end
