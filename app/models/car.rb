class Car < ApplicationRecord
  belongs_to :organization, optional: true
  has_and_belongs_to_many :drivers
  has_and_belongs_to_many :routes
  has_many :issues
end
