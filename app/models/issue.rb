class Issue < ApplicationRecord
  belongs_to :car, optional: true
  belongs_to :schedule
end
