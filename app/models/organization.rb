class Organization < ApplicationRecord
  has_many :cars
  has_many :drivers
  has_many :schedules
  has_and_belongs_to_many :accounts
end
