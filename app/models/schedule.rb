class Schedule < ApplicationRecord
  belongs_to :route
  belongs_to :organization
  has_many :check_points
  has_many :issues
end
