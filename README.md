# Route schedule [link](https://blg-bus.ru) 

System for automated generation of bus schedules.

Version list:

* Ruby - 3.0.4
* Rails - 7.0.4
* PostgreSQL - 1.4
* Webpacker - 6.0.0
* Vue - 3.2.45
* Vuetify - 3.1.5