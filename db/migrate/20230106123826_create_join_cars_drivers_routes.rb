class CreateJoinCarsDriversRoutes < ActiveRecord::Migration[7.0]
  def self.up
    create_join_table :drivers, :cars do |t|
      t.index [:driver_id, :car_id]
    end

    create_join_table :routes, :cars do |t|
      t.index [:route_id, :car_id]
    end

    add_reference :cars, :organization, index: true
    add_reference :drivers, :organization, index: true
  end

  def self.down
    drop_table :cars_drivers
    drop_table :cars_routes

    remove_reference :drivers, :organization, index: true
    remove_reference :cars, :organization, index: true
  end
end
