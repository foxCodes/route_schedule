class CreateRoutes < ActiveRecord::Migration[7.0]
  def self.up
    create_table :routes do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :routes
  end
end
