class AddRouteToSchedule < ActiveRecord::Migration[7.0]
  def self.up
    add_reference :schedules, :route, index: true
    add_reference :check_points, :schedule, index: true
  end

  def self.down
    remove_reference :schedules, :route, index: true
    remove_reference :check_points, :schedule, index: true
  end
end
