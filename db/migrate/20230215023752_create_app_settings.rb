class CreateAppSettings < ActiveRecord::Migration[7.0]
  def self.up
    create_table :app_settings do |t|
      t.integer :full_page_width
      t.integer :default_break_circle
      t.integer :default_count_circle
      t.integer :add_time_weekend
      t.integer :count_replacement_issues
      t.boolean :stream_table_page

      t.timestamps
    end
  end

  def self.down
    drop_table :app_settings
  end
end
