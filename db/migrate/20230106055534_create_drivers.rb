class CreateDrivers < ActiveRecord::Migration[7.0]
  def self.up
    create_table :drivers do |t|
      t.string :surname
      t.string :name
      t.string :patronymic
      t.date :birthday
      t.string :sex

      t.timestamps
    end
  end

  def self.down
    drop_table :drivers
  end
end
