class CreateIssues < ActiveRecord::Migration[7.0]
  def self.up
    create_table :issues do |t|
      t.integer :number
      t.string :start_side
      t.integer :break_circle
      t.integer :count_circle
      t.time :start_time
      t.integer :replace_issue
      t.boolean :is_active
      t.belongs_to :schedule, index: true
      t.belongs_to :car, index: true

      t.timestamps
    end
  end

  def self.down
    drop_table :issues
  end
end
