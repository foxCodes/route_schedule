class CreateSchedules < ActiveRecord::Migration[7.0]
  def self.up
    create_table :schedules do |t|
      t.string :name
      t.integer :time_circle
      t.integer :count_issue
      t.date :begin_date
      t.date :end_date
      t.time :begin_time
      t.time :end_time
      t.boolean :weekend
      t.boolean :is_active

      t.timestamps
    end
  end

  def self.down
    drop_table :schedules
  end
end
