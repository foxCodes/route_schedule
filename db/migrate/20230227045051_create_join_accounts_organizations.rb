class CreateJoinAccountsOrganizations < ActiveRecord::Migration[7.0]
  def self.up
    create_join_table :accounts, :organizations do |t|
      t.index [:account_id, :organization_id]
    end

    add_reference :schedules, :organization, index: true
    add_column :accounts, :authentication_token, :string, limit: 30
    add_index :accounts, :authentication_token, unique: true
  end

  def self.down
    remove_reference :schedules, :organization, index: true
    remove_column :accounts, :authentication_token

    drop_table :accounts_organizations
  end
end
