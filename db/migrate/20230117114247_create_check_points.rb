class CreateCheckPoints < ActiveRecord::Migration[7.0]
  def self.up
    create_table :check_points do |t|
      t.string :name
      t.integer :time_next_point
      t.integer :number_pint
      t.boolean :expand, default: false
      t.boolean :menu, default: false

      t.timestamps
    end
  end

  def self.down
    drop_table :check_points
  end

end
