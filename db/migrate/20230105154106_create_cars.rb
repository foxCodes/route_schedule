class CreateCars < ActiveRecord::Migration[7.0]
  def self.up
    create_table :cars do |t|
      t.string :make
      t.string :model
      t.string :license_plate_number

      t.timestamps
    end
  end

  def self.down
    drop_table :cars
  end
end
