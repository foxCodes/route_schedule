# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_02_27_045051) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.string "surname"
    t.string "patronymic"
    t.date "birthday"
    t.string "sex"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30
    t.index ["authentication_token"], name: "index_accounts_on_authentication_token", unique: true
    t.index ["email"], name: "index_accounts_on_email", unique: true
    t.index ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true
  end

  create_table "accounts_organizations", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "organization_id", null: false
    t.index ["account_id", "organization_id"], name: "index_accounts_organizations_on_account_id_and_organization_id"
  end

  create_table "accounts_roles", id: false, force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "role_id", null: false
    t.index ["account_id", "role_id"], name: "index_accounts_roles_on_account_id_and_role_id"
  end

  create_table "app_settings", force: :cascade do |t|
    t.integer "full_page_width"
    t.integer "default_break_circle"
    t.integer "default_count_circle"
    t.integer "add_time_weekend"
    t.integer "count_replacement_issues"
    t.boolean "stream_table_page"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cars", force: :cascade do |t|
    t.string "make"
    t.string "model"
    t.string "license_plate_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "organization_id"
    t.index ["organization_id"], name: "index_cars_on_organization_id"
  end

  create_table "cars_drivers", id: false, force: :cascade do |t|
    t.bigint "driver_id", null: false
    t.bigint "car_id", null: false
    t.index ["driver_id", "car_id"], name: "index_cars_drivers_on_driver_id_and_car_id"
  end

  create_table "cars_routes", id: false, force: :cascade do |t|
    t.bigint "route_id", null: false
    t.bigint "car_id", null: false
    t.index ["route_id", "car_id"], name: "index_cars_routes_on_route_id_and_car_id"
  end

  create_table "check_points", force: :cascade do |t|
    t.string "name"
    t.integer "time_next_point"
    t.integer "number_pint"
    t.boolean "expand", default: false
    t.boolean "menu", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "schedule_id"
    t.index ["schedule_id"], name: "index_check_points_on_schedule_id"
  end

  create_table "drivers", force: :cascade do |t|
    t.string "surname"
    t.string "name"
    t.string "patronymic"
    t.date "birthday"
    t.string "sex"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "organization_id"
    t.index ["organization_id"], name: "index_drivers_on_organization_id"
  end

  create_table "issues", force: :cascade do |t|
    t.integer "number"
    t.string "start_side"
    t.integer "break_circle"
    t.integer "count_circle"
    t.time "start_time"
    t.integer "replace_issue"
    t.boolean "is_active"
    t.bigint "schedule_id"
    t.bigint "car_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["car_id"], name: "index_issues_on_car_id"
    t.index ["schedule_id"], name: "index_issues_on_schedule_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "spec_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "routes", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.string "name"
    t.integer "time_circle"
    t.integer "count_issue"
    t.date "begin_date"
    t.date "end_date"
    t.time "begin_time"
    t.time "end_time"
    t.boolean "weekend"
    t.boolean "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "route_id"
    t.bigint "organization_id"
    t.index ["organization_id"], name: "index_schedules_on_organization_id"
    t.index ["route_id"], name: "index_schedules_on_route_id"
  end

end
