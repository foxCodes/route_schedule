# Директория, которая содержит проект
#app_dir = File.expand_path("../..", __FILE__)

#current_dir = "#{app_dir}/current"
#shared_dir = "#{app_dir}/shared"



# Change to match your CPU core count
# Приложение - набор файлов. Фалы: испольняемые(выгружаются в ОП) и ресурсные(файлы бд).
# Процесс - экземпляр запущеного приложения. Поток - экземпляр конкретно ветки исполнения приложения.
# Соответствует количеству физических ядер в процессоре. Задает процессы.
# grep -c processor /proc/cpuinfo
#workers 2
workers 1
#workers ENV.fetch("WEB_CONCURRENCY") { 2 }



# Min and Max threads per worker
# Минимальное и максимальное значение пула потоков.
threads 5, 5

#max_threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }
#min_threads_count = ENV.fetch("RAILS_MIN_THREADS") { max_threads_count }
#threads min_threads_count, max_threads_count



# Specifies the `port` that Puma will listen on to receive requests; default is 3000.
port ENV.fetch("PORT") { 3000 }



# Specifies the `environment` that Puma will run in.
environment ENV.fetch("RAILS_ENV") { "production" }



# Use the `preload_app!` method when specifying a `workers` number.
# This directive tells Puma to first boot the application and load code
# before forking the application. This takes advantage of Copy On Write
# process behavior so workers use less memory.
preload_app!



# Allow puma to be restarted by `rails restart` command.
#plugin :tmp_restart



# Set up socket location
# Настройка для сокета UNIX. Позволяет общаться с nginx.
#bind "unix:#{shared_dir}/tmp/sockets/puma.sock"
#bind "unix:/data/srv/vits.amursu.ru/shared/tmp/sockets/puma.sock"
bind "unix:/data/srv/blg-bus.ru/shared/tmp/sockets/puma.sock"



# Specifies the `pidfile` that Puma will use.
#pidfile "#{shared_dir}/tmp/pids/server.pid"
#pidfile "/data/srv/vits.amursu.ru/shared/tmp/pids/server.pid"
pidfile "/data/srv/blg-bus.ru/shared/tmp/pids/server.pid"



# Set state locations
#state_path "#{shared_dir}/tmp/pids/puma.state"
#state_path "/data/srv/vits.amursu.ru/shared/tmp/pids/puma.state"
state_path "/data/srv/blg-bus.ru/shared/tmp/pids/puma.state"



# Logging
#stdout_redirect "#{shared_dir}/log/puma.stdout.log", "#{shared_dir}/log/puma.stderr.log", true
#stdout_redirect "/data/srv/vits.amursu.ru/shared/log/puma.stdout.log", "/data/srv/vits.amursu.ru/shared/log/puma.stderr.log", true
stdout_redirect "/data/srv/blg-bus.ru/shared/log/puma.stdout.log", "/data/srv/blg-bus.ru/shared/log/puma.stderr.log", true



on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection(YAML.load_file("/data/srv/blg-bus.ru/shared/config/database.yml")["production"])
end