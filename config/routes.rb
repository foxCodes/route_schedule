Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  #root "application#index"
  get 'app', to: 'application#index'

  #get "/organizations", controller: :organizations, action: :index
  resources :organizations
  resources :app_settings
  resources :check_points
  resources :schedules
  resources :drivers
  resources :issues
  resources :routes
  resources :cars

  get 'issue_destroy_by', controller: :issues, action: :destroy_by
  get 'generation_issues', controller: :issues, action: :generation

  get 'cars_by_sign', controller: :cars, action: :index_by_sign
  get 'issues_by_sign', controller: :issues, action: :index_by_sign
  get 'drivers_by_sign', controller: :drivers, action: :index_by_sign
  get 'check_points_by_sign', controller: :check_points, action: :index_by_sign
  #get 'routes_by_sign', controller: :drivers, action: :index_by_sign
  # get 'auth_test_token' => 'accounts#auth_test_token', :as => 'auth_test_token'

  get '/accounts' => 'accounts#index', :as => 'accounts_registration'
  post '/accounts' => 'accounts#create', :as => 'create_account_registration'
  put '/accounts/:id' => 'accounts#update', :as => 'update_account_registration'
  delete '/account/:id' => 'accounts#destroy', :as => 'destroy_account_registration'
  get '/create_password' => 'accounts#create_password', :as => 'password_account_registration'
  get 'current_acccout_date', controller: :application, action: :current_acccout_date
  get '/roles', controller: :accounts, action: :role_index



  #devise_for :accounts
  devise_for :accounts, skip: [:registrations]#,  controllers: { registrations: "accounts/registrations" }
  #as :accounts do
  #  get '/accounts' => 'accounts/registrations#index', :as => 'accounts_registration_path'
  #  post '/accounts' => 'devise/registrations#create', :as => 'create_account_registration_path'
  #  put '/accounts/:id' => 'devise/registrations#update', :as => 'update_account_registration_path'
  #  delete '/accounts/:id' => 'devise/registrations#destroy', :as => 'destroy_account_registration_path'
  #end

  devise_scope :account do
    root to: "devise/sessions#new"
    #authenticated :account do
    # get '/accounts' => 'accounts/registrations#index', :as => 'accounts_registration'
    # post '/accounts' => 'accounts/registrations#create', :as => 'create_account_registration' не работает для дублирующего контролера
    # put '/accounts/:id' => 'accounts/registrations#update', :as => 'update_account_registration'
    # delete '/accounts/:id' => 'accounts/registrations#destroy', :as => 'destroy_account_registration'
    # get '/create_password' => 'accounts/registrations#create_password', :as => 'password_account_registration'
    #end

=begin
    resource :accounts,
           only: [:index, :create, :update, :destroy],
           controller: 'accounts/registrations',
           as: :account_registration do
              get 'index'
    end
=end
  end
end
