const { webpackConfig, merge } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')

vueConfig = {
    resolve: {
        extensions: [ '.vue']
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: { loader: 'vue-loader' }
            },
        ]
    },
    plugins: [new VueLoaderPlugin()]
}

module.exports = merge(vueConfig, webpackConfig)